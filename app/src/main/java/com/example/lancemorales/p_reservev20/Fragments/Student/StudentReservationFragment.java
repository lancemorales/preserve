package com.example.lancemorales.p_reservev20.Fragments.Student;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.lancemorales.p_reservev20.Utilities.AlertDialogManager;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageStudentFragment;
import com.example.lancemorales.p_reservev20.Model.StudentReservationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentReservationFragment extends Fragment {

    TimePicker timeFrom, timeTo;
    EditText etRoom, etSection;
    Button btContinue;
    AlertDialogManager alert = new AlertDialogManager();
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    String studentNumber;
    ProgressDialog progressDialog;

    int reservationCount = 0;
    final StudentReservationModel studentReserve = new StudentReservationModel();

    public StudentReservationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragmentView = inflater.inflate(R.layout.fragment_student_reservation, container, false);

        progressDialog = new ProgressDialog(getContext());
        firebaseAuth = FirebaseAuth.getInstance();
        databaseRef = FirebaseDatabase.getInstance().getReference();

        timeFrom = (TimePicker) fragmentView.findViewById(R.id.timeFrom);
        timeTo = (TimePicker) fragmentView.findViewById(R.id.timeTo);
        etRoom = (EditText) fragmentView.findViewById(R.id.etRoomReserve);
        etSection = (EditText) fragmentView.findViewById(R.id.etSectionReserve);
        btContinue = (Button) fragmentView.findViewById(R.id.btContinueToDialogStud);


//        User is already logged in.
        if (firebaseAuth.getCurrentUser() != null){
            FirebaseUser user = firebaseAuth.getCurrentUser();
            databaseRef = FirebaseDatabase.getInstance().getReference().
                    child(user.getUid()).child("Account").child("studentNumber");
//            Log.e("TESTING", databaseRef + " ");
            databaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    studentNumber = dataSnapshot.getValue().toString();
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });

            databaseRef = FirebaseDatabase.getInstance().getReference().
                    child(user.getUid()).child("Account").child("reservationCount");
//            Log.e("TESTING", databaseRef + " ");

            databaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    reservationCount = Integer.parseInt(dataSnapshot.getValue().toString());

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
        }
        submitReservation();
        return fragmentView;
    }

    public void submitReservation(){
        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etRoom.getText().toString().trim().equals("") || etSection.getText().toString().trim().equals("")) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Please fill up all fields", false);
                    return;
                }

                if (etRoom.getText().toString().length() > 3) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Invalid room number", false);
                    return;
                }

                if (etRoom.getText().toString().length() > 4) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Invalid section", false);
                    return;
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Submit Reservation?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    reservationOfStudent();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
    }

    public void reservationOfStudent(){
        int fromHour = 0;
        int fromMin = 0;
        int toHour = 0;
        int toMin = 0;

        //Put a validation if room is a number.
//        if(etRoom.getText().toString().trim().equals("")
//                ||etSection.getText().toString().trim().equals("")) {
//            alert.showAlertDialog(getContext(), "Reservation failed", "Please fill up all fields", false);
//            return;
//        }
//
//        else{
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                String id = getActivity().getIntent().getStringExtra("userId");
//                SharedPreferences sp = getSharedPreferences("MyUserID", Context.MODE_PRIVATE);
//                String idNum = sp.getString("userID", "0000000000");
//                String idStudNum = helper.getStudentNumber(idNum);

                String idNum = "";
                String idStudNum = "";
                String fromFormat, toFormat;
                fromHour = timeFrom.getHour();
                fromMin = timeFrom.getMinute();
                toHour = timeTo.getHour();
                toMin = timeTo.getMinute();
                int roomNumber = Integer.parseInt(etRoom.getText().toString());
                String section = etSection.getText().toString();

                if (fromHour == 0) {
                    fromHour += 12;
                    fromFormat = "AM";
                }
                else if (fromHour == 12) {
                    fromFormat = "PM";
                } else if (fromHour > 12) {
                    fromHour -= 12;
                    fromFormat = "PM";
                } else {
                    fromFormat = "AM";
                }

                if (toHour == 0) {
                    toHour += 12;
                    toFormat = "AM";
                }
                else if (toHour == 12) {
                    toFormat = "PM";
                } else if (toHour > 12) {
                    toHour -= 12;
                    toFormat = "PM";
                } else {
                    toFormat = "AM";
                }

                if (reservationCount != 0){
                    alert.showAlertDialog(getContext()   ,
                            "Reserving projector Failed", "You have a pending reservation.", false);
                }

                else if (reservationCount == 0){
                    insertReservation(studentNumber, fromHour, fromMin, fromFormat, toHour, toMin, toFormat, roomNumber, section);
                }

        }
    }

    public void insertReservation(String studNum, int fromHour, int fromMin, String fromDayStamp, int toHour, int toMin,
           String toDayStamp ,int room, String section){
        final StudentReservationModel studInfo = new StudentReservationModel(studNum, fromHour, fromMin, fromDayStamp,
                toHour, toMin, toDayStamp, room, section);

        progressDialog.setMessage("Reserving your projector ...");
        progressDialog.show();

        final FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference();

        databaseRef.child(user.getUid()).child("Reservation").push().setValue(studInfo)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getActivity().getApplicationContext(), "Projector reservation submitted", Toast.LENGTH_LONG).show();
                        //Update Reservation count here.
                        reservationCount++;
//                        databaseRef.child(user.getUid()).child("Account").child("reservationCount").setValue(reservationCount).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
////                                Toast.makeText(getActivity().getApplicationContext(), "ReservationCount Updated", Toast.LENGTH_LONG).show();
//                                progressDialog.cancel();
//                            }
//                        });

                        databaseRef.child(user.getUid()).child("Account").child("reservationCount").setValue(reservationCount).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
//                                Toast.makeText(getActivity().getApplicationContext(), "ReservationCount Updated", Toast.LENGTH_LONG).show();
                                progressDialog.cancel();
                            }
                        });
                    }
                });;
        //Redirect to Welcome Page
        backToWelcomeStudent();
    }

    public void backToWelcomeStudent(){
        WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(welcomePageStudentFragment, "welcomePageStudentFragment")
                .replace(R.id.fragment_layout, welcomePageStudentFragment)
                .addToBackStack("welcomePageStudentFragment")
                .commit();
    }

}
