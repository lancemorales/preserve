package com.example.lancemorales.p_reservev20.Fragments.Registration;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.lancemorales.p_reservev20.R;

public class RegisterFragment extends Fragment {



    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_register, container, false);

        Button btnBackToMain = (Button) fragmentView.findViewById(R.id.btBackToMain);
        Button btnRegAdmin = (Button) fragmentView.findViewById(R.id.btRegAdmin);
        Button btnRegStudent = (Button) fragmentView.findViewById(R.id.btRegStudent);

        btnRegAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toRegAdmin();
            }
        });

        btnRegStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toRegStudent();
            }
        });

        return fragmentView;
    }

    public void toRegAdmin(){

        RegisterAdminFragment registerAdminFragment = new RegisterAdminFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(registerAdminFragment, "registerAdminFragment")
                .replace(R.id.fragment_layout, registerAdminFragment)
                .addToBackStack("registerAdminFragment")
                .commit();
    }

    public void toRegStudent(){

        RegisterStudentFragment registerStudentFragment = new RegisterStudentFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(registerStudentFragment, "registerStudentFragment")
                .replace(R.id.fragment_layout, registerStudentFragment)
                .addToBackStack("registerStudentFragment")
                .commit();
    }
}
