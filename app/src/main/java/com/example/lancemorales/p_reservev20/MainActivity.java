package com.example.lancemorales.p_reservev20;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.example.lancemorales.p_reservev20.Utilities.AlertDialogManager;
import com.example.lancemorales.p_reservev20.Fragments.MainFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class MainActivity extends AppCompatActivity{

    Button btLogin, btRegister, btSignIn;
    EditText userID, userPass, idNumStudNum, pass;
    AlertDialogManager alert = new AlertDialogManager();
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        firebaseAuth = FirebaseAuth.getInstance();
//
////        User is already logged in.
//        if (firebaseAuth.getCurrentUser() != null){
//            FirebaseUser user = firebaseAuth.getCurrentUser();
//            databaseRef = FirebaseDatabase.getInstance().getReference().
//                    child(user.getUid()).child("Account").child("userType");
//            databaseRef.addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    // This method is called once with the initial value and again
//                    // whenever data at this location is updated.
//                    String userType = dataSnapshot.getValue().toString();
//                    Log.e("TESTING", " " + userType);
//                    if (userType == "0"){
//                        //go to admin
//                        Log.e("TESTING123", " " + userType);
//
//                        startActivity(new Intent(getApplicationContext(), WelcomePageAdmin.class));
//
//                    }
//
//                    else if (userType == "1"){
//                        Log.e("TESTING1234", " " + userType);
//
//                        startActivity(new Intent(getApplicationContext(), WelcomePageStudent.class));
//                    }
//                    finish();
//                }
//
//                @Override
//                public void onCancelled(DatabaseError error) {
//                    // Failed to read value
//                    Log.w("TAG", "Failed to read value.", error.toException());
//                }
//            });
//        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btLogin=(Button)findViewById(R.id.btLogin);
        btSignIn = (Button) findViewById(R.id.btNextToWelcomePage);
        btRegister=(Button)findViewById(R.id.btRegister);
        userID = (EditText) findViewById(R.id.etLoginIDStudNum);
        userPass = (EditText) findViewById(R.id.etPasswordLogin);
        idNumStudNum = (EditText) findViewById(R.id.etLoginIDStudNum);
        pass = (EditText) findViewById(R.id.etPasswordLogin);

        willView();

    }

    public void willView() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_layout, new MainFragment()).commit();
    }

}
