package com.example.lancemorales.p_reservev20.Fragments.Admin;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lancemorales.p_reservev20.Adapters.CustomAdapterAdmin;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageAdminFragment;
import com.example.lancemorales.p_reservev20.Model.StudentInformationModel;
import com.example.lancemorales.p_reservev20.Model.StudentReservationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminViewReservationsFragment extends Fragment {

    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    ArrayList<StudentInformationModel> studentInformationModels = new ArrayList<>();
    ArrayList<StudentReservationModel> studentReservationModels = new ArrayList<>();

    public AdminViewReservationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragmentView = inflater.inflate(R.layout.fragment_admin_view_reservations, container, false);

        ListView userListView = (ListView) fragmentView.findViewById(R.id.listViewAllReservation);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference();
        Log.e("TEST", " " + databaseRef.toString());
        final CustomAdapterAdmin customAdapter = new CustomAdapterAdmin(getContext(), studentInformationModels, studentReservationModels);

        userListView.setAdapter(customAdapter);

        databaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println(dataSnapshot.getValue());

                if (Integer.parseInt(String.valueOf(dataSnapshot.child("Account")
                        .child("reservationCount").getValue())) != 0){
                        StudentInformationModel studentInformationModel = new StudentInformationModel();
                        StudentReservationModel studentReservationModel = new StudentReservationModel();
                        int x = 0;
                        studentInformationModel.setFullName((String) dataSnapshot.child("Account").child("fullName").getValue());
                        studentInformationModel.setStudentNumber(String.valueOf(dataSnapshot.child("Account").child("studentNumber").getValue()) );
    //                        studentInformationModel.setStudSection((String) dataSnapshot.child("Account").child("course").getValue());
                        studentInformationModel.setReserverID((String) dataSnapshot.getKey());
                        studentInformationModel.setReservationCount(Integer.parseInt(String.valueOf(dataSnapshot.child("Account")
                                .child("reservationCount").getValue())));
                        for(DataSnapshot postSnapshot: dataSnapshot.child("Reservation").getChildren()){
                            studentInformationModel.setReserveKey(postSnapshot.getKey().toString());

                            studentInformationModel.setStudSection(postSnapshot.child("studSection").getValue().toString());
                            String fromHour = String.valueOf(postSnapshot.child("fromHourValue").getValue());
                            String fromMins = String.valueOf(postSnapshot.child("fromMinValue").getValue());
                            String fromDayStamp = String.valueOf(postSnapshot.child("fromDayStamp").getValue());
                            studentInformationModel.setFromTime(fromHour+":"+fromMins+fromDayStamp);

                            String toHour = String.valueOf(postSnapshot.child("toHourValue").getValue());
                            String toMins = String.valueOf(postSnapshot.child("toMinValue").getValue());
                            String toDayStamp = String.valueOf(postSnapshot.child("toDayStamp").getValue());
                            studentInformationModel.setToTime(toHour+":"+toMins+toDayStamp);
                            Log.e("Start Time", studentInformationModel.getToTime());

                            if(Integer.parseInt(String.valueOf(dataSnapshot.child("Account")
                                    .child("reservationCount").getValue())) != 1){
                                StudentInformationModel studentInformationModel2 = new StudentInformationModel();
                                studentInformationModel2.setReserveKey(postSnapshot.getKey().toString());
                                studentInformationModel2.setStudSection(postSnapshot.child("studSection").getValue().toString());
                                studentInformationModel2.setFullName((String) dataSnapshot.child("Account").child("fullName").getValue());
                                studentInformationModel2.setStudentNumber(String.valueOf(dataSnapshot.child("Account").child("studentNumber").getValue()) );
                                studentInformationModel2.setReserverID((String) dataSnapshot.getKey());
                                studentInformationModel2.setReservationCount(Integer.parseInt(String.valueOf(dataSnapshot.child("Account")
                                        .child("reservationCount").getValue())));
                                String hour2 = String.valueOf(postSnapshot.child("hourValue").getValue());
                                String mins2 = String.valueOf(postSnapshot.child("minValue").getValue());
                                String dayStamp2 = String.valueOf(postSnapshot.child("dayStamp").getValue());
                                studentInformationModel2.setFromTime(hour2+":"+mins2+dayStamp2);
                                studentInformationModels.add(studentInformationModel2);
                            }

                            else break;
                        }

    //                        studentReservationModels.add(studentReservationModel);
                        x++;
                        if (Integer.parseInt(String.valueOf(dataSnapshot.child("Account")
                                .child("reservationCount").getValue())) == 1){
                            studentInformationModels.add(studentInformationModel);

                        }
                        customAdapter.notifyDataSetChanged();


                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Toast.makeText(getActivity().getApplicationContext(), "Reservation Updated", Toast.LENGTH_LONG).show();
//                startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageAdmin.class));

                WelcomePageAdminFragment welcomePageAdminFragment = new WelcomePageAdminFragment();
                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                fragTransaction.add(welcomePageAdminFragment, "welcomePageAdminFragment")
                        .replace(R.id.fragment_layout, welcomePageAdminFragment)
                        .addToBackStack("welcomePageAdminFragment")
                        .commit();


                customAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return fragmentView;
    }

}
