package com.example.lancemorales.p_reservev20.Model;

/**
 * Created by Raeven on 23 Mar 2017.
 */

public class StudentInformationModel  {

    public int userType;
    public String studentNumber;
    public String lastName;
    public String firstName;
    public String course;
    public String fullName;
    public int reservationCount;
    //For Reservation Queries
    public String section;
    public String fromTime;
    public String toTime;
    public String reserveKey;
    public String reserverID;
    public String studSection;


    public StudentInformationModel(String studentNumber, String lastName,
                                   String firstName, String course, String fullName){
        this.setUserType(1); //This is for student
        this.setStudentNumber(studentNumber);
        this.setLastName(lastName);
        this.setFirstName(firstName);
        this.setCourse(course);
        this.setFullName(fullName);
        this.setReservationCount(0);
    }

    public StudentInformationModel(){

    }


    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getReservationCount() {
        return reservationCount;
    }

    public void setReservationCount(int reservationCount) {
        this.reservationCount = reservationCount;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getReserveKey() {
        return reserveKey;
    }

    public void setReserveKey(String reserveKey) {
        this.reserveKey = reserveKey;
    }

    public String getReserverID() {
        return reserverID;
    }

    public void setReserverID(String reserverID) {
        this.reserverID = reserverID;
    }

    public String getStudSection() {
        return studSection;
    }

    public void setStudSection(String studSection) {
        this.studSection = studSection;
    }
}
