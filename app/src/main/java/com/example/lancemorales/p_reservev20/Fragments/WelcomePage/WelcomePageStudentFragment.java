package com.example.lancemorales.p_reservev20.Fragments.WelcomePage;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lancemorales.p_reservev20.Fragments.Student.StudentReservationFragment;
import com.example.lancemorales.p_reservev20.Fragments.Student.StudentViewReservationFragment;
import com.example.lancemorales.p_reservev20.MainActivity;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomePageStudentFragment extends Fragment {

    TextView tvId;
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    Button btnPlaceAReservation, btnLogout, btnCheckReservations;

    public WelcomePageStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_welcome_page_student, container, false);

        btnCheckReservations = (Button) fragmentView.findViewById(R.id.btCheckStudReservation);
        btnLogout = (Button) fragmentView.findViewById(R.id.btLogoutStudent);
        btnPlaceAReservation = (Button) fragmentView.findViewById(R.id.btCreateReservationStudent);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();


        tvId = (TextView) fragmentView.findViewById(R.id.tvDisplayStudNum);
        databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
                .child("Account").child("fullName");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String fullName = dataSnapshot.getValue().toString();
                tvId.setText(fullName);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("TAG", "Failed to read value.", error.toException());
            }
        });
        String idNum = user.getEmail();
        getOnClickListeners();

        return fragmentView;
    }

    public void getOnClickListeners(){
        btnPlaceAReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StudentReservationFragment studentReservationFragment = new StudentReservationFragment();
                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                fragTransaction.add(studentReservationFragment, "studentReservationFragment")
                        .replace(R.id.fragment_layout, studentReservationFragment)
                        .addToBackStack("studentReservationFragment")
                        .commit();
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
                getActivity().finish();
                startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
            }
        });

        btnCheckReservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StudentViewReservationFragment studentViewReservationFragment = new StudentViewReservationFragment();
                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                fragTransaction.add(studentViewReservationFragment, "studentViewReservationFragment")
                        .replace(R.id.fragment_layout, studentViewReservationFragment)
                        .addToBackStack("studentViewReservationFragment")
                        .commit();
            }
        });
    }


}
