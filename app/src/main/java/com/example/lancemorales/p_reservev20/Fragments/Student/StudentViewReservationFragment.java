package com.example.lancemorales.p_reservev20.Fragments.Student;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.lancemorales.p_reservev20.Adapters.CustomAdapterStudent;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageStudentFragment;
import com.example.lancemorales.p_reservev20.Model.StudentReservationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentViewReservationFragment extends Fragment {

    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    ListView userListView;
    ImageView imageView_Cancel;
    ArrayList<StudentReservationModel> studentReservationModels = new ArrayList<>();
    FirebaseUser user;

    public StudentViewReservationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_student_view_reservation, container, false);

        userListView = (ListView) fragmentView.findViewById(R.id.listViewReservation);

        imageView_Cancel = (ImageView) fragmentView.findViewById(R.id.imageView_Cancel);
        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
                .child("Reservation");

        reserveThis();
        return fragmentView;
    }

    public void reserveThis(){
        final CustomAdapterStudent customAdapter = new CustomAdapterStudent(getContext(), studentReservationModels);
        userListView.setAdapter(customAdapter);
        final StudentReservationModel studentReserve = new StudentReservationModel();

        databaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                  Log.e("TESTING", " " + dataSnapshot.());

                studentReserve.setRoomNumber(Integer.parseInt(dataSnapshot.child("roomNumber").getValue().toString()));
                studentReserve.setStudSection((String) dataSnapshot.child("studSection").getValue());
                String fromHour = String.valueOf(dataSnapshot.child("fromHourValue").getValue());
                String fromMin = String.valueOf(dataSnapshot.child("fromMinValue").getValue());
                String fromDayStamp = (String) dataSnapshot.child("fromDayStamp").getValue();

                String toHour = String.valueOf(dataSnapshot.child("toHourValue").getValue());
                String toMin = String.valueOf(dataSnapshot.child("toMinValue").getValue());
                String toDayStamp = (String) dataSnapshot.child("toDayStamp").getValue();
//                  studentReserve.setReservationCount();
                studentReserve.setFromTime(fromHour+":"+fromMin+fromDayStamp);
                studentReserve.setToTime(toHour+":"+toMin+toDayStamp);
                studentReserve.setStudSection((String) dataSnapshot.child("studSection").getValue());
                studentReserve.setReserveKey((String) dataSnapshot.getKey());
                studentReserve.setStatus((String) dataSnapshot.child("status").getValue());

                databaseRef = FirebaseDatabase.getInstance().getReference().
                        child(user.getUid()).child("Account").child("reservationCount");
                databaseRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        studentReserve.setReservationCount(Integer.valueOf(dataSnapshot.getValue().toString()));
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("TAG", "Failed to read value.", error.toException());
                    }
                });

                studentReservationModels.add(studentReserve);
                customAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                customAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Toast.makeText(getContext(), "Reservation Updated", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageStudent.class));

//                final FragmentManager fragmentManager = getFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, new WelcomePageStudentFragment()).commit();

                try{
                    StudentViewReservationFragment studentViewReservationFragment = new StudentViewReservationFragment();
                    FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                    fragTransaction.add(studentViewReservationFragment, "studentViewReservationFragment")
                            .detach(studentViewReservationFragment)
                            .commit();

                    WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
                    FragmentTransaction fragTransaction1 = getFragmentManager().beginTransaction();
                    fragTransaction1.add(welcomePageStudentFragment, "welcomePageStudentFragment")
                            .replace(R.id.fragment_layout, welcomePageStudentFragment)
                            .addToBackStack("welcomePageStudentFragment")
                            .commit();


                    customAdapter.notifyDataSetChanged();

                } catch(Exception e){
                    Log.d("FAKKING EXCEP", "this" + e);
                }



//                WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
//                //May bug dito
//
//                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
//                fragTransaction.add(welcomePageStudentFragment, "welcomePageStudentFragment")
//                        .replace(R.id.fragment_layout, welcomePageStudentFragment)
//                        .addToBackStack("welcomePageStudentFragment")
//                        .commit();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                customAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                customAdapter.notifyDataSetChanged();

            }
        });
    }



}
