package com.example.lancemorales.p_reservev20.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lancemorales.p_reservev20.Model.StudentReservationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Raeven on 26 Mar 2017.
 */

public class CustomAdapterStudent extends ArrayAdapter<StudentReservationModel> {
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    public CustomAdapterStudent(Context context, ArrayList<StudentReservationModel> reserveInfo) {
        super(context, 0, reserveInfo);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){


//        databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
//                .child("Reservation");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.record_template, parent, false);
        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
                ;
//        StudentInformationModel from = getItem(position);
        final StudentReservationModel to = getItem(position);
        TextView tvRoom = (TextView) customView.findViewById(R.id.tvRoom);
        TextView tvTimeFrom = (TextView) customView.findViewById(R.id.tvStartTime);
        TextView tvTimeTo = (TextView) customView.findViewById(R.id.tvEndTime);
        TextView tvStudSect = (TextView) customView.findViewById(R.id.tvSectionStud);
        TextView tvKey = (TextView) customView.findViewById(R.id.tvKeyStudent);
        TextView tvStatus = (TextView) customView.findViewById(R.id.tvStatus);

//        TextView tvSection_Temp = (TextView) customView.findViewById(R.id.tv_endTime_reserve);
//        TextView tvKey = (TextView) customView.findViewById(R.id.tvKey);
//
        tvRoom.setText(String.valueOf(to.getRoomNumber()));
        tvTimeFrom.setText(to.getFromTime());
        tvTimeTo.setText(to.getToTime());
        tvStudSect.setText(to.getStudSection());
        tvKey.setText(to.getReserveKey());
        tvStatus.setText(to.getStatus());

        final String testing = to.getReserveKey();
        ImageView imageCancel = (ImageView) customView.findViewById(R.id.imageView_Cancel);

        imageCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseRef.child(testing).addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        int count = to.getReservationCount();
//                        databaseRef.child("Reservation").child(testing).removeValue();
//                        String testing2 = databaseRef.child("Account").
//                                child("reservationCount").toString();
//
//                        if (testing2 != "0"){
//                            databaseRef.child("Account").child("reservationCount").setValue(count -1);
//                        }
//
//                        notifyDataSetChanged();
//                    }
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        int count = to.getReservationCount();

                        if (count != 0){
//                            int count = studentReserve.getReservationCount();

                            databaseRef.child("Reservation").child(testing).removeValue();
                            //                        int testing = (databaseRef.child("reservationCount"));
                            databaseRef.child("Account").child("reservationCount").setValue(count -1);
                        } else{
                            databaseRef.child("Reservation").child(testing).removeValue();
                            //                        int testing = (databaseRef.child("reservationCount"));
                            databaseRef.child("Account").child("reservationCount").setValue(0);
                        }

////                        int count = to.getReservationCount();
//                        databaseRef.child("Reservation").child(testing).removeValue();
//                    //                        int testing = (databaseRef.child("reservationCount"));
//                        databaseRef.child("Account").child("reservationCount").setValue(count -1);

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
//                firebaseAuth = FirebaseAuth.getInstance();
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                databaseRef = FirebaseDatabase.getInstance().getReference();
//                databaseRef.child(user.getUid()).child("Reservation").child(testing).removeValue();
//                notifyDataSetChanged();
//
//                Log.e("TESTING KEY", "" + testing);
            }
        });
        return customView;
    }
}
