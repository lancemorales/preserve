package com.example.lancemorales.p_reservev20.Fragments.Login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.lancemorales.p_reservev20.R;


public class LoginFragment extends Fragment {

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);

        Button btnLoginAdmin = (Button) fragmentView.findViewById(R.id.btLoginShellAdmin);
        Button btnLoginStudent = (Button) fragmentView.findViewById(R.id.btLoginShellStudent);

        btnLoginAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginAdmin();
            }
        });

        btnLoginStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginStudent();
            }
        });

        return fragmentView;
    }

    public void loginAdmin(){
        LoginAdminAndStudentFragment loginAdminAndStudentFragment = new LoginAdminAndStudentFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(loginAdminAndStudentFragment, "loginAdminFragment")
                .replace(R.id.fragment_layout, loginAdminAndStudentFragment)
                .addToBackStack("loginAdminFragment")
                .commit();
    }

    public void loginStudent(){
        LoginAdminAndStudentFragment loginAdminAndStudentFragment = new LoginAdminAndStudentFragment();
        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.add(loginAdminAndStudentFragment, "loginAdminAndStudentFragment")
                .replace(R.id.fragment_layout, loginAdminAndStudentFragment)
                .addToBackStack("loginAdminAndStudentFragment")
                .commit();
    }

}
