package com.example.lancemorales.p_reservev20.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.lancemorales.p_reservev20.Fragments.Login.LoginAdminAndStudentFragment;
import com.example.lancemorales.p_reservev20.Fragments.Login.LoginFragment;
import com.example.lancemorales.p_reservev20.Fragments.Registration.RegisterFragment;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageAdminFragment;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageStudentFragment;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainFragment extends Fragment {

    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);

        Button btnRegister = (Button) fragmentView.findViewById(R.id.btRegister);
        Button btnLogin = (Button) fragmentView.findViewById(R.id.btLogin);

        checkUser();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toRegister();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLogin();
            }
        });

        return fragmentView;
    }

    public void checkUser(){
        firebaseAuth = FirebaseAuth.getInstance();

//        User is already logged in.
        if (firebaseAuth.getCurrentUser() != null){
            FirebaseUser user = firebaseAuth.getCurrentUser();
            databaseRef = FirebaseDatabase.getInstance().getReference().
                    child(user.getUid()).child("Account").child("userType");
            databaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    String userType = dataSnapshot.getValue().toString();
                    if (userType == "0"){
                        //go to admin

//                        startActivity(new Intent(getApplicationContext(), WelcomePageAdmin.class));

                        WelcomePageAdminFragment welcomePageAdminFragment = new WelcomePageAdminFragment();
                        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                        fragTransaction.add(welcomePageAdminFragment, "welcomePageAdminFragment")
                                .replace(R.id.fragment_layout, welcomePageAdminFragment)
                                .addToBackStack("welcomePageAdminFragment")
                                .commit();

                    }

                    else if (userType == "1"){
                        Log.e("TESTING1234", " " + userType);

//                        startActivity(new Intent(getApplicationContext(), WelcomePageStudent.class));

                        WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
                        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                        fragTransaction.add(welcomePageStudentFragment, "welcomePageStudentFragment")
                                .replace(R.id.fragment_layout, welcomePageStudentFragment)
                                .addToBackStack("welcomePageStudentFragment")
                                .commit();
                    }
//                    finish();
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
        }
    }

    public void toRegister(){
//        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_layout, new RegisterFragment()).commit();

        RegisterFragment registerFragment = new RegisterFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(registerFragment, "registerFragment")
                        .replace(R.id.fragment_layout, registerFragment)
                        .addToBackStack("registerFragment")
                        .commit();

    }

    public void toLogin(){
//        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_layout, new LoginFragment()).commit();

//        LoginFragment loginFragment = new LoginFragment();
        LoginAdminAndStudentFragment loginAdminAndStudentFragment = new LoginAdminAndStudentFragment();
        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
        fragTransaction.add(loginAdminAndStudentFragment, "loginAdminAndStudentFragment")
                .replace(R.id.fragment_layout, loginAdminAndStudentFragment)
                .addToBackStack("loginAdminAndStudentFragment")
                .commit();
    }
}
