package com.example.lancemorales.p_reservev20.Fragments.Login;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lancemorales.p_reservev20.Utilities.AlertDialogManager;
import com.example.lancemorales.p_reservev20.Utilities.DatabaseHelper;
import com.example.lancemorales.p_reservev20.Utilities.SessionManager;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageAdminFragment;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageStudentFragment;
import com.example.lancemorales.p_reservev20.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginAdminAndStudentFragment extends Fragment {

    EditText studNum, studPass;
    DatabaseHelper helper;
    Button btLogin, btBack;
    SessionManager session;
    FirebaseAuth firebaseAuth;
    AlertDialogManager alert = new AlertDialogManager();
    ProgressDialog progressDialog;
    DatabaseReference databaseRef;

    public LoginAdminAndStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_login_student, container, false);

        studNum = (EditText) fragmentView.findViewById(R.id.etStudNumLogin);
        studPass = (EditText) fragmentView.findViewById(R.id.etStudPassLogin);
        btLogin = (Button) fragmentView.findViewById(R.id.btNextToWelcomeStudent);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(getContext());

        signInStudent();

        return fragmentView;
    }

    public void signInStudent() {

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String studEmail = studNum.getText().toString().trim() + "@ust.edu.ph";
                String studPassword = studPass.getText().toString().trim();

                if (TextUtils.isEmpty(studEmail)) {
                    alert.showAlertDialog(getContext(), "Login failed", "Please enter your student number.", false);
                    return;
                } else if (TextUtils.isEmpty(studPassword)) {
                    alert.showAlertDialog(getContext(), "Login failed", "Please enter your password.", false);
                    return;
                }

                progressDialog.setMessage("Please Wait ...");
                progressDialog.show();

                firebaseAuth.signInWithEmailAndPassword(studEmail, studPassword)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (task.isSuccessful()) {
                                    Toast.makeText(getActivity().getApplicationContext(), "Login Successful", Toast.LENGTH_LONG).show();

                                    FirebaseUser user = firebaseAuth.getCurrentUser();
                                    databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
                                            .child("Account").child("userType");
                                    Log.e("TESTING", databaseRef + " ");
                                    databaseRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            // This method is called once with the initial value and again
                                            // whenever data at this location is updated.
                                            String userType = dataSnapshot.getValue().toString();
                                            Log.e("TESTING", " " + userType);
                                            if (userType == "0") {
                                                //go to admin
                                                Log.e("TESTING123", " " + userType);
//                                                startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageAdminFragment.class));

                                                WelcomePageAdminFragment welcomePageAdminFragment = new WelcomePageAdminFragment();
                                                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                                                fragTransaction.add(welcomePageAdminFragment, "welcomePageAdminFragment")
                                                        .replace(R.id.fragment_layout, welcomePageAdminFragment)
                                                        .addToBackStack("welcomePageAdminFragment")
                                                        .commit();
                                                progressDialog.dismiss();

                                            } else if (userType == "1") {
                                                Log.e("TESTING1234", " " + userType);
//                                                startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageStudentFragment.class));

                                                WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
                                                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                                                fragTransaction.add(welcomePageStudentFragment, "welcomePageStudentFragment")
                                                        .replace(R.id.fragment_layout, welcomePageStudentFragment)
                                                        .addToBackStack("welcomePageStudentFragment")
                                                        .commit();
                                                progressDialog.dismiss();

                                            }
//                                            getActivity().finish();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError error) {
                                            // Failed to read value
                                            Log.w("TAG", "Failed to read value.", error.toException());
                                        }
                                    });
                                } else if (!task.isSuccessful()) {
//                            finish();
                                    //Forward to Dashboard
                                    alert.showAlertDialog(getContext(), "Login failed", "Incorrect ID number or password", false);
                                    progressDialog.dismiss();
//                                    LoginAdminAndStudentFragment loginAdminAndStudentFragment = new LoginAdminAndStudentFragment();
//                                    FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
//                                    fragTransaction.add(loginAdminAndStudentFragment, "loginAdminAndStudentFragment")
//                                            .replace(R.id.fragment_layout, loginAdminAndStudentFragment)
//                                            .addToBackStack("loginAdminAndStudentFragment")
//                                            .commit();
                                }

                            }
                        });

            }

        });
    }
}
