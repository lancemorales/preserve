package com.example.lancemorales.p_reservev20.Model;

/**
 * Created by Raeven on 24 Mar 2017.
 */

public class AdminInformationModel {

    public int userType;
    public String adminNumber;
    public String lastName;
    public String firstName;
    public String fullName;
    public int reservationCount;

    public AdminInformationModel(String adminNumber, String lastName,
                                   String firstName, String fullName){
        this.setUserType(0); //This is for admin
        this.setAdminNumber(adminNumber);
        this.setLastName(lastName);
        this.setFirstName(firstName);
        this.setFullName(fullName);
        this.setReservationCount(0);
    }

    public AdminInformationModel(){

    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAdminNumber() {
        return adminNumber;
    }

    public void setAdminNumber(String adminNumber) {
        this.adminNumber = adminNumber;
    }

    public int getReservationCount() {
        return reservationCount;
    }

    public void setReservationCount(int reservationCount) {
        this.reservationCount = reservationCount;
    }
}
