package com.example.lancemorales.p_reservev20.Fragments.Registration;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lancemorales.p_reservev20.Utilities.AlertDialogManager;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageStudentFragment;
import com.example.lancemorales.p_reservev20.Model.StudentInformationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterStudentFragment extends Fragment {

    EditText etStudNumber, etPasswordStud, etConfirmPassStud, etLastnameStud, etFirstnameStud, etCourseStud;
    AlertDialogManager alert = new AlertDialogManager();
    ProgressDialog progressDialog;
    Button btnRegisterStudent;

    DatabaseReference databaseRef;
    FirebaseAuth firebaseAuth;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    public RegisterStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragmentView = inflater.inflate(R.layout.fragment_register_student, container, false);
        etStudNumber = (EditText) fragmentView.findViewById(R.id.etStudNumReg);
        etPasswordStud = (EditText) fragmentView.findViewById(R.id.etPasswordStudReg);
        etConfirmPassStud = (EditText) fragmentView.findViewById(R.id.etConfirmPassStud);
        etLastnameStud = (EditText) fragmentView.findViewById(R.id.etStudLastnameReg);
        etFirstnameStud = (EditText) fragmentView.findViewById(R.id.etStudFirstnameReg);
        etCourseStud = (EditText) fragmentView.findViewById(R.id.etCourseStudReg);
        btnRegisterStudent = (Button) fragmentView.findViewById(R.id.btNextToWelcomeStud);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(getContext());

        databaseRef = FirebaseDatabase.getInstance().getReference();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        registerStudent();


        return fragmentView;
    }

    public void registerStudent(){
        btnRegisterStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etStudNumber.getText().toString().trim().equals("") || etPasswordStud.getText().toString().trim().equals("") ||
                        etConfirmPassStud.getText().toString().trim().equals("") || etLastnameStud.getText().toString().trim().equals("") ||
                        etFirstnameStud.getText().toString().trim().equals("")) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Please fill up all fields", false);
                    return;
                } else if (!etPasswordStud.getText().toString().equals(etConfirmPassStud.getText().toString())) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Passwords does not match", false);
                    return;
                }
                else if (etPasswordStud.getText().toString().length() != 6 ){
                    alert.showAlertDialog(getContext(), "Registration failed", "Password must have a minimum of 6 characters", false);
                    return;
                }
                else {
                    progressDialog.setMessage("Registering User ...");
                    progressDialog.show();


                    String studNumber = etStudNumber.getText().toString() + "@ust.edu.ph";
                    String studPass = etPasswordStud.getText().toString();

                    firebaseAuth.createUserWithEmailAndPassword(studNumber, studPass)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                progressDialog.cancel();
                                registerStudentInformation();
                                Toast.makeText(getActivity().getApplicationContext(), "Account Saved", Toast.LENGTH_LONG).show();
//                                        getActivity().finish();
//                                        startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageStudentFragment.class));

                                WelcomePageStudentFragment welcomePageStudentFragment = new WelcomePageStudentFragment();
                                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                                fragTransaction.add(welcomePageStudentFragment, "welcomePageStudentFragment")
                                        .replace(R.id.fragment_layout, welcomePageStudentFragment)
                                        .addToBackStack("welcomePageStudentFragment")
                                        .commit();

                                //Redirect to LoginAdminAndStudentFragment Page
                            } else {
                                progressDialog.cancel();

                                Toast.makeText(getActivity().getApplicationContext(), "Register Failed " +
                                        task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

    }


    public void registerStudentInformation() {
        String studNumber = etStudNumber.getText().toString().trim();
        String lastName = etLastnameStud.getText().toString().trim();
        String firstName = etFirstnameStud.getText().toString().trim();
        String course = etFirstnameStud.getText().toString().trim();
        String fullName = firstName + " " + lastName;

        StudentInformationModel studInfo = new StudentInformationModel(studNumber, lastName, firstName
                , course, fullName);
        FirebaseUser user = firebaseAuth.getCurrentUser();

        databaseRef.child(user.getUid()).child("Account").setValue(studInfo)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getActivity().getApplicationContext(), "Details Saved", Toast.LENGTH_LONG).show();
                    }
                });
    }

}
