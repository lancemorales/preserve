package com.example.lancemorales.p_reservev20.Fragments.WelcomePage;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lancemorales.p_reservev20.Utilities.SessionManager;
import com.example.lancemorales.p_reservev20.Fragments.Admin.AdminViewReservationsFragment;
import com.example.lancemorales.p_reservev20.MainActivity;
import com.example.lancemorales.p_reservev20.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomePageAdminFragment extends Fragment {


    SessionManager session;
    Button btnViewReservations, btnLogout;
    TextView tvID;

    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;

    public WelcomePageAdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragmentView = inflater.inflate(R.layout.fragment_welcome_page_admin, container, false);

        session = new SessionManager(getActivity().getApplicationContext());
        tvID = (TextView) fragmentView.findViewById(R.id.tvDisplayAdminNum);
        btnLogout = (Button) fragmentView.findViewById(R.id.btLogoutAdmin);
        btnViewReservations = (Button) fragmentView.findViewById(R.id.btViewReservationsAdmin);

        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference().child(user.getUid())
                .child("Account").child("fullName");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String fullName = dataSnapshot.getValue().toString();
                tvID.setText(fullName);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("TAG", "Failed to read value.", error.toException());
            }
        });

        logoutOrViewReservations();

        return fragmentView;
    }

    public void logoutOrViewReservations(){
        btnViewReservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AdminViewReservationsFragment adminViewReservationsFragment = new AdminViewReservationsFragment();
                FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                fragTransaction.add(adminViewReservationsFragment, "adminViewReservationsFragment")
                        .replace(R.id.fragment_layout, adminViewReservationsFragment)
                        .addToBackStack("adminViewReservationsFragment")
                        .commit();
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
//                getActivity().finish();
                startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
            }
        });
    }
}
