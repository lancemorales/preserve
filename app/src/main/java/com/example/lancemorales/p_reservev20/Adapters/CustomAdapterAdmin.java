package com.example.lancemorales.p_reservev20.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.lancemorales.p_reservev20.R;
import com.example.lancemorales.p_reservev20.Model.StudentInformationModel;
import com.example.lancemorales.p_reservev20.Model.StudentReservationModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class CustomAdapterAdmin extends ArrayAdapter<StudentInformationModel>
{
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseRef;
    public CustomAdapterAdmin(Context context, ArrayList<StudentInformationModel> studentInfo
                        , ArrayList<StudentReservationModel> reserveInfo) {
        super(context, 0, studentInfo);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View customView = inflater.inflate(R.layout.record_template_admin, parent, false);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference()
                ;

        final StudentInformationModel from = getItem(position);
//        StudentReservationModel to = getItem(position);

        TextView tvName = (TextView) customView.findViewById(R.id.tvFullName);
        TextView tvStudNum = (TextView) customView.findViewById(R.id.tvStudentNumber);
        TextView tvStudSect = (TextView) customView.findViewById(R.id.tvSectionStud);
        TextView tvStartTime = (TextView) customView.findViewById(R.id.tvStartTime);
        TextView tvEndTime = (TextView) customView.findViewById(R.id.tvEndTime);
        TextView tvKey = (TextView) customView.findViewById(R.id.tvKey);

        tvName.setText(from.getFullName());
        tvStudNum.setText(from.getStudentNumber());
        tvStudSect.setText(from.getStudSection());
        tvStartTime.setText(String.valueOf(from.getFromTime()));
        tvEndTime.setText(String.valueOf(from.getToTime()));
        tvKey.setText(from.getReserveKey());
        Button buttonReject = (Button) customView.findViewById(R.id.button_Reject);
        Button buttonReserve = (Button) customView.findViewById(R.id.button_Reserve);

        Log.e("TESTING FUCK", " "+  from.getReserverID());
        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = from.getReservationCount();
                Log.e("Testing", "" + count);


                databaseRef.child(from.getReserverID()).child("Reservation")
                        .child(from.getReserveKey()).child("status").setValue("REJECTED");
                databaseRef.child(from.getReserverID()).child("Account")
                        .child("reservationCount").setValue((count-1));

            }
        });

        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = from.getReservationCount();
                Log.e("Testing", "" + count);

                databaseRef.child(from.getReserverID()).child("Reservation")
                        .child(from.getReserveKey()).child("status").setValue("RESERVED");
                databaseRef.child(from.getReserverID()).child("Account")
                        .child("reservationCount").setValue((count-1));
            }
        });
//        tvRoom.setText(to.getDayStamp());
        return customView;
    }
}
