package com.example.lancemorales.p_reservev20.Model;

/**
 * Created by Raeven on 24 Mar 2017.
 */

public class StudentReservationModel {

    public String studNum;
    public int fromHourValue;
    public int fromMinValue;
    public int toHourValue;
    public int toMinValue;
    public String fromDayStamp;
    public String toDayStamp;
    public int roomNumber;
    public String studSection;
    public String status;
    public String reserveKey;
    public String fromTime;
    public String toTime;

    public int reservationCount;
    public StudentReservationModel(String studNum, int fromHour, int fromMin, String fromDayStamp, int toHour, int toMin,
                                   String toDayStamp,int room, String section){
        this.setStudNum(studNum);
        this.setFromHourValue(fromHour);
        this.setFromMinValue(fromMin);
        this.setToHourValue(toHour);
        this.setToMinValue(toMin);
        this.setFromDayStamp(fromDayStamp);
        this.setToDayStamp(toDayStamp);
        this.setRoomNumber(room);
        this.setStudSection(section);
        this.setStatus("PENDING");
    }

    public StudentReservationModel(){

    }

    public String getStudNum() {
        return studNum;
    }

    public void setStudNum(String studNum) {
        this.studNum = studNum;
    }

    public String getStudSection() {
        return studSection;
    }

    public void setStudSection(String studSection) {
        this.studSection = studSection;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getToMinValue() {
        return toMinValue;
    }

    public void setToMinValue(int toMinValue) {
        this.toMinValue = toMinValue;
    }

    public int getToHourValue() {
        return toHourValue;
    }

    public void setToHourValue(int toHourValue) {
        this.toHourValue = toHourValue;
    }

    public String getToDayStamp() {
        return toDayStamp;
    }

    public void setToDayStamp(String toDayStamp) {
        this.toDayStamp = toDayStamp;
    }

    public int getFromMinValue() {
        return fromMinValue;
    }

    public void setFromMinValue(int fromMinValue) {
        this.fromMinValue = fromMinValue;
    }

    public int getFromHourValue() {
        return fromHourValue;
    }

    public void setFromHourValue(int fromHourValue) {
        this.fromHourValue = fromHourValue;
    }

    public String getFromDayStamp() {
        return fromDayStamp;
    }

    public void setFromDayStamp(String fromDayStamp) {
        this.fromDayStamp = fromDayStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReserveKey() {
        return reserveKey;
    }

    public void setReserveKey(String reserveKey) {
        this.reserveKey = reserveKey;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public int getReservationCount() {
        return reservationCount;
    }

    public void setReservationCount(int reservationCount) {
        this.reservationCount = reservationCount;
    }

}
