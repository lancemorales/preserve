package com.example.lancemorales.p_reservev20.Fragments.Registration;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lancemorales.p_reservev20.Utilities.AlertDialogManager;
import com.example.lancemorales.p_reservev20.Fragments.WelcomePage.WelcomePageAdminFragment;
import com.example.lancemorales.p_reservev20.Model.AdminInformationModel;
import com.example.lancemorales.p_reservev20.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterAdminFragment extends Fragment {

    EditText etIDNumber, etPassword, etConfirmPass, etLastname, etFirstname;
    AlertDialogManager alert = new AlertDialogManager();

    ProgressDialog progressDialog;

    DatabaseReference databaseRef;
    Button btnRegisterAdmin;
    FirebaseAuth firebaseAuth;

    public RegisterAdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(getContext());

        databaseRef = FirebaseDatabase.getInstance().getReference();

        View fragmentView = inflater.inflate(R.layout.fragment_register_admin, container, false);

        btnRegisterAdmin = (Button) fragmentView.findViewById(R.id.btNextToWelcomePage);
        etIDNumber = (EditText) fragmentView.findViewById(R.id.etIDNumRegAdmin);
        etPassword = (EditText) fragmentView.findViewById(R.id.etPasswordRegAdmin);
        etConfirmPass = (EditText) fragmentView.findViewById(R.id.etConfirmPassAdmin);
        etLastname = (EditText) fragmentView.findViewById(R.id.etLastnameAdminReg);
        etFirstname = (EditText) fragmentView.findViewById(R.id.etFirstnameAdminReg);
        registerAdmin();

        return fragmentView;
    }

    public void registerAdmin(){
        btnRegisterAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etIDNumber.getText().toString().trim().equals("") || etPassword.getText().toString().trim().equals("") ||
                        etConfirmPass.getText().toString().trim().equals("") || etLastname.getText().toString().trim().equals("") ||
                        etFirstname.getText().toString().trim().equals("")) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Please fill up all fields", false);
                    return;
                }
                else if (!etPassword.getText().toString().equals(etConfirmPass.getText().toString())) {
                    alert.showAlertDialog(getContext(), "Registration failed", "Passwords does not match", false);
                    return;
                }

                else if (etPassword.getText().toString().length() != 6 ){
                    alert.showAlertDialog(getContext(), "Registration failed", "Password must have a minimum of 6 characters", false);
                    return;
                }

                else {
                    progressDialog.setMessage("Registering User ...");
                    progressDialog.show();

                    String adminNumber = etIDNumber.getText().toString() + "@ust.edu.ph";
                    String adminPass = etPassword.getText().toString();

                    firebaseAuth.createUserWithEmailAndPassword(adminNumber, adminPass)
                            .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {

                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.cancel();
                                        registerAdminInformation();
                                        Toast.makeText(getActivity().getApplicationContext(), "Account Saved", Toast.LENGTH_LONG).show();
//                                        getActivity().finish();
//                                        startActivity(new Intent(getActivity().getApplicationContext(), WelcomePageAdminF.class));

                                        WelcomePageAdminFragment welcomePageAdminFragment = new WelcomePageAdminFragment();
                                        FragmentTransaction fragTransaction = getFragmentManager ().beginTransaction();
                                        fragTransaction.add(welcomePageAdminFragment, "welcomePageAdminFragment")
                                                .replace(R.id.fragment_layout, welcomePageAdminFragment)
                                                .addToBackStack("welcomePageAdminFragment")
                                                .commit();

                                        //Redirect to Login Page
                                    } else {
                                        progressDialog.cancel();
                                        Toast.makeText(getActivity().getApplicationContext(), "Register Failed " +
                                                task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();                                    }

                                }
                            });
                }
            }
        });
    }

    public void registerAdminInformation(){
        String studNumber = etIDNumber.getText().toString().trim();
        String lastName = etLastname.getText().toString().trim();
        String firstName = etFirstname.getText().toString().trim();
//        String course = etFirstnameStud.getText().toString().trim();
        String fullName = firstName + " " + lastName;

        AdminInformationModel studInfo = new AdminInformationModel(studNumber, lastName, firstName
                , fullName);

        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef.child(user.getUid()).child("Account").setValue(studInfo);
        Toast.makeText(getActivity().getApplicationContext(), "Details Saved", Toast.LENGTH_LONG).show();
    }

}
